#!/bin/sh
##
## Written for Debian on behalf of the Debian Science Team
## by Jerome Benoit <calculus@rezozer.net>, 2014-10-07.
##
## Examples either extracted from the source ball README
## or communnicated by the upstream maintainer.
##

SYMPOW=/usr/bin/sympow

[ -x $SYMPOW ] || exit 1

SYMPOW_OPTS='-verbose'

### computation samples
$SYMPOW $SYMPOW_OPTS -sp 2p16 -curve "[1,2,3,4,5]"
#$SYMPOW $SYMPOW_OPTS -sp 3p12d2,4p8 -curve "[1,2,3,4,5]" ## heavy sample
$SYMPOW $SYMPOW_OPTS -sp 3p12d2,4p8 -curve "[0,1,2,3,4]"  ## light alternative
$SYMPOW $SYMPOW_OPTS -sp 3p12d2,4p8 -curve "[1,1,0,1,0]"  ## light alternative
$SYMPOW $SYMPOW_OPTS -sp 7p12d1 -hecke -curve "[0,0,1,-16758,835805]"
$SYMPOW $SYMPOW_OPTS -sp 2bp16 -curve "[1,2,3,4,5]"
$SYMPOW $SYMPOW_OPTS -curve "[1,2,3,4,5]" -moddeg
$SYMPOW $SYMPOW_OPTS -curve "[1,2,3,4,5]" -analrank
$SYMPOW $SYMPOW_OPTS -curve "[0,0,1,-79,342]" -analrank
$SYMPOW $SYMPOW_OPTS -sp 2bp64 -curve '[0,0,0,0,5]'
$SYMPOW $SYMPOW_OPTS -sp 1w0p6D0,1w0p6D2,1w0p6D4,1w0p6D6,1w0p6D8 -curve "[1,1,0,1,0]"

### inverse Mellin transform mesh computation samples
$SYMPOW $SYMPOW_OPTS -new_data 2
$SYMPOW $SYMPOW_OPTS -new_data 3d2
$SYMPOW $SYMPOW_OPTS -new_data 6d0h
$SYMPOW $SYMPOW_OPTS -new_data 4c

exit 0
## eos
